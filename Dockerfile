# Stage 1: build static files
FROM node:16.18.1-alpine as build-static

WORKDIR /travelwala-frontend

COPY tsconfig.json ./
COPY public/ ./public

COPY package.json ./
RUN [ "npm", "install" ]

COPY src/ ./src

# RUN [ "npm", "run", "build" ]
ENTRYPOINT [ "npm", "start" ]

# Stage 2: run nginx
# FROM nginx:alpine

# # COPY nginx.conf /etc/nginx/nginx.conf

# RUN rm -rf /usr/share/nginx/html/*

# COPY --from=build-static /travelwala-frontend/build/ /usr/share/nginx/html

# EXPOSE 3000 80

# ENTRYPOINT ["nginx", "-g", "daemon off;"]